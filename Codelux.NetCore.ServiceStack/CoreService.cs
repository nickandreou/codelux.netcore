﻿using ServiceStack;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using Codelux.NetCore.Common.Requests;
using Codelux.NetCore.Common.Responses;

namespace Codelux.NetCore.ServiceStack
{
    public sealed class CoreService : Service
    {
        public Task<VersionResponse> Get(VersionRequest request)
        {
            Assembly assembly = Assembly.GetCallingAssembly();
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);

            return Task.FromResult(new VersionResponse()
            {
                Version = versionInfo
            });
        }
    }
}
