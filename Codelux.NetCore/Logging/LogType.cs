﻿namespace Codelux.NetCore.Logging
{
    public enum LogType
    {
        Info,
        Warn,
        Error,
        Debug,
    }
}
