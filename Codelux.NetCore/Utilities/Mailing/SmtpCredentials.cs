﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Codelux.NetCore.Utilities.Mailing
{
    public class SmtpCredentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
