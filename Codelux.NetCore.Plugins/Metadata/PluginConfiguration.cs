﻿using Newtonsoft.Json;

namespace Codelux.NetCore.Plugins.Metadata
{
    public class PluginConfiguration
    {
        public PluginHeader Header { get; set; }
        public Type PluginType { get; set; }
    }
}
