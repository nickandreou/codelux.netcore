﻿using Codelux.NetCore.Plugins.Metadata;
using Codelux.NetCore.Runnables;

namespace Codelux.NetCore.Plugins.Base
{
    public interface IPlugin : IRunnable
    {
        void Configure(PluginConfiguration configuration);
    }
}
