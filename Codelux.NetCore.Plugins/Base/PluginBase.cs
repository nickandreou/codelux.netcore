﻿using Codelux.NetCore.Runnables;
using Codelux.NetCore.Plugins.Metadata;

namespace Codelux.NetCore.Plugins.Base
{
    public abstract class PluginBase : Runnable, IPlugin
    {
        public PluginConfiguration Configuration { get; set; }

        protected PluginBase() { }

        public abstract void Configure(PluginConfiguration configuration);
    }
}
